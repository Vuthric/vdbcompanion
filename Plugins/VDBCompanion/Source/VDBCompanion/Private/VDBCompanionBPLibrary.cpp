// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "VDBCompanionBPLibrary.h"
#include "VDBCompanion.h"
#include "Engine/Texture.h"
THIRD_PARTY_INCLUDES_START
#include <openvdb/tools/ChangeBackground.h>
THIRD_PARTY_INCLUDES_END

UVDBCompanionBPLibrary::UVDBCompanionBPLibrary(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{

}

float UVDBCompanionBPLibrary::VDBCompanionSampleFunction(float Param)
{
	return -1;
}

bool UVDBCompanionBPLibrary::ImportVDBFileToVolumeTexture(UVolumeTexture* VolumeTexture, FString VDBPath, FIntVector Start, FIntVector End, float ValueScale, ETextureSourceFormat TextureFormat)
{
	openvdb::initialize();

	openvdb::io::File file(std::string(TCHAR_TO_UTF8(*VDBPath)));
	file.open();

	openvdb::GridBase::Ptr baseGrid;
//	baseGrid = file.readGrid(file.beginName().gridName());
	baseGrid = file.readGrid("density");

	for (openvdb::io::File::NameIterator nameIter = file.beginName(); nameIter != file.endName(); ++nameIter)
	{
//		UE_LOG(LogTemp, Warning, TEXT("%s"), *nameIter.gridName().c_str());
	}
	file.close();

	openvdb::FloatGrid::Ptr grid = openvdb::gridPtrCast<openvdb::FloatGrid>(baseGrid);

	const float outside = grid->background();
	//const float width = 2.0 * outside;

/*
	for (openvdb::FloatGrid::ValueOnIter i = grid->beginValueOn(); i; ++i)
	{
		float dist = i.getValue();
		i.setValue((outside - dist) / width);

		i.setValue(1.0);
	}
	for (openvdb::FloatGrid::ValueOffIter i = grid->beginValueOff(); i; ++i)
	{
		if (i.getValue() < 0.0)
		{
 			i.setValue(1.0);
			i.setValueOff();
		}
		i.setValue(0.0);
		i.setValueOff();

	}
*/
	openvdb::tools::changeBackground(grid->tree(), 0.0);
	openvdb::FloatGrid::Accessor accessor = grid->getAccessor();

	openvdb::CoordBBox vdbBox;
	vdbBox = grid->evalActiveVoxelBoundingBox();
	openvdb::CoordBBox readBox(Start.X, Start.Y, Start.Z, End.X, End.Y, End.Z);
	if (vdbBox.isInside(readBox))
	{
		auto QueryVoxel = [&](const int32 x, const int32 y, const int32 z, void* ret)
		{
			openvdb::Coord xyz(x + Start.X, y + Start.Y, z + Start.Z);

			if (TextureFormat == ETextureSourceFormat::TSF_G8)
			{
				uint8* const Voxel = static_cast<uint8*>(ret);
				Voxel[0] = accessor.getValue(xyz) * 256 * ValueScale;
			}
			else
			{
				half* const Voxel = static_cast<half*>(ret);
				Voxel[0] = accessor.getValue(xyz) * ValueScale;
				Voxel[1] = 0;
				Voxel[2] = 0;
				Voxel[3] = 0;
			}
/*			if (accessor.getValue(xyz) > 0)
			{
				UE_LOG(LogTemp, Warning, TEXT("(%s, %s, %s): %s"),
					*FString::FromInt(xyz.x()),
					*FString::FromInt(xyz.y()),
					*FString::FromInt(xyz.z()),
					*FString::SanitizeFloat(accessor.getValue(xyz)));
			}
*/
		};

		//openvdb::Vec3d gridSize = grid->voxelSize();
		// 	VolumeTexture->UpdateSourceFromFunction(QueryVoxel, gridSize[0], gridSize[1], gridSize[2]);
		VolumeTexture->UpdateSourceFromFunction(QueryVoxel, End.X - Start.X, End.Y - Start.Y, End.Z - Start.Z, TextureFormat);
		VolumeTexture->MarkPackageDirty();

		return true;

	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Tried to read box %s - %s which is not inside bound box of the VDB file %s - %s."),
			*FIntVector(readBox.min().x(), readBox.min().y(), readBox.min().z()).ToString(),
			*FIntVector(readBox.max().x(), readBox.max().y(), readBox.max().z()).ToString(),
			*FIntVector(vdbBox.min().x(), vdbBox.min().y(), vdbBox.min().z()).ToString(),
			*FIntVector(vdbBox.max().x(), vdbBox.max().y(), vdbBox.max().z()).ToString()
			);
		return false;
	}
}

